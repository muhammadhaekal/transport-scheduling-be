// Import express package (Framework to create REST API)
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// Initiate app using express
const app = express();

// Handle all request from localhost:3000/employees
const employees = require("./api/employees");
// Handle all request from localhost:3000/category_colors
const categoryColors = require("./api/category_colors");
// Handle all request from localhost:3000/members
const members = require("./api/members");
// Handle all request from localhost:3000/drivers
const drivers = require("./api/drivers");
// Handle all request from localhost:3000/vehicles
const vehicles = require("./api/vehicles");
// Handle all request from localhost:3000/appointments
const appointments = require("./api/appointments");

// Import mongoose library (connector between mongoDB and JS)
const mongoose = require("mongoose");

// Configure monggose to connect with local mongoDB database
mongoose.connect("mongodb://localhost:27017/myapp", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

app.use(cors());
// This 2 lines of code enable BE to read request body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Handle all request from localhost:3000/employees
app.use("/employees", employees);
// Handle all request from localhost:3000/category_colors
app.use("/category_colors", categoryColors);
// Handle all request from localhost:3000/members
app.use("/members", members);
// Handle all request from localhost:3000/drivers
app.use("/drivers", drivers);
// Handle all request from localhost:3000/vehicles
app.use("/vehicles", vehicles);
// Handle all request from localhost:3000/appointments
app.use("/appointments", appointments);

app.get("/", (req, res) => {
  res.send("Hai Guys");
});

// Port setting
app.listen(3000, () => console.log(`Example app listening on port 3000! `));
