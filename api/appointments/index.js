const express = require("express");
const router = express.Router();
// Import controller from ./controller.js file
const controller = require("./controller");

// handle request to GET localhost:3000/vehicles (get all vehicles)
router.get("/", controller.getAll);
router.get(
  "/get_appointment_by_month/:month/:year",
  controller.getAppointmentsByMonth
);
// handle request to GET localhost:3000/vehicles/:id (get vehicle by id)
router.get("/:id", controller.getById);
// handle request to POST localhost:3000/vehicles (insert one vehicle data)
router.post("/", controller.createOne);
// handle request to PUT localhost:3000/vehicles/:id (edit vehicle data by id)
router.put("/:id", controller.editById);
// handle request to DELETE localhost:3000/vehicles/:id (delete vehicle data by id)
router.delete("/:id", controller.deleteById);
// handle request to DELETE localhost:3000/vehicles (delete all vehicle data)
router.delete("/", controller.deleteAll);

module.exports = router;
