// Import model from ./model.js
const Appointments = require("./model");
const CategoryColors = require("../category_colors/model");
const moment = require("moment")
require("moment-recur")

// Function to get all appointments
const getAll = (req, res) => {
  Appointments.find({}, null, { sort: { date_from: "asc" } })
    .populate({
      path: "vehicle_id",
      populate: {
        path: "category_color_id",
      },
    })
    .populate({
      path: "category_color_id",
    })
    .populate({
      path: "driver_id",
    })
    .populate({
      path: "members",
    })
    .exec((err, appointments) => {
      res.send({
        success: true,
        appointments: appointments,
      });
    });
};

// Function to insert one appointment data
const createOne = async (req, res) => {
  const newAppointment = new Appointments(req.body);
  const startDate = new Date(req.body.date_from);
  const endDate = new Date(req.body.date_to);
  const fromHour = startDate.getHours()
  const fromMinute = startDate.getMinutes()
  const toHour = endDate.getHours()
  const toMinute = endDate.getMinutes()

  let category = await CategoryColors.findOne({ _id: req.body.category_color_id }).exec(async (err, color) => {

    if (color.category === "onetime") {
      Appointments.findOne({
        $or: [
          { date_from: { $gte: startDate, $lte: endDate } },
          { date_to: { $gte: startDate, $lte: endDate } },
        ],
      })
        .populate({
          path: "members",
        })
        .exec((err, appointment) => {
          if (err)
            return res.send({
              success: false,
              message: err.message,
            });
          if (appointment) {
            let isMemberExist = false;
            let memberName = "";

            for (i = 0; i < appointment.members.length; i++) {
              for (j = 0; j < req.body.members.length; j++) {
                if (appointment.members[i]._id.toString() === req.body.members[j]) {
                  memberName = `${appointment.members[i].first_name} ${appointment.members[i].last_name}`;
                  isMemberExist = true;
                }
              }
            }
            if (isMemberExist) {
              return res.send({
                success: false,
                message: `member already booked in another activity`,
              });
            }
            if (appointment.driver_id.toString() === req.body.driver_id) {
              return res.send({
                success: false,
                message: `Driver already booked`,
              });
            }
            if (appointment.vehicle_id.toString() === req.body.vehicle_id) {
              return res.send({
                success: false,
                message: `Vehicle already booked`,
              });
            }
          }
          newAppointment.save((err, newData) => {
            if (err)
              return res.send({
                success: false,
                message: err.message,
              });

            res.send({
              new_appointment: newData,
              success: true,
            });
          });
        });
    } else {
      let reccurObj = {
        daily: 60,
        weekly: 52,
        fortnightly: 26,
        monthly: 52,
        yearly: 1
      }
      let recur = null
      let dates = []
      console.log(color.category);

      if (color.category === "daily") {
        recur = moment(startDate).recur().every().days(1)
        dates = recur.next(reccurObj[color.category], "YYYY-MM-DD")
      } else if (color.category === "weekly") {
        recur = moment(startDate).recur().every().weeks(1)
        dates = recur.next(reccurObj[color.category], "YYYY-MM-DD")
      } else if (color.category === "fortnightly") {
        recur = moment(startDate).recur().every().weeks(2)
        dates = recur.next(reccurObj[color.category], "YYYY-MM-DD")
      } else if (color.category === "monthly") {
        recur = moment(startDate).recur().every().month(1)
        dates = recur.next(reccurObj[color.category], "YYYY-MM-DD")
      } else if (color.category === "yearly") {
        recur = moment(startDate).recur().every().year(1)
        dates = recur.next(reccurObj[color.category], "YYYY-MM-DD")
      }

      let successWithSomeError = false
      const currDateISOFormat = await moment(startDate).set({ hour: fromHour, minute: fromMinute })
        .toISOString()

      dates.push(currDateISOFormat)
      await Promise.all(dates.map(async (date, index) => {
        const dateISOFormatTo = await moment(date).set({ hour: fromHour, minute: fromMinute })
          .toISOString()
        const dateISOFormatToTo = await moment(date).set({ hour: toHour, minute: toMinute })
          .toISOString()
        await Appointments.findOne({
          $or: [
            { date_from: { $gte: dateISOFormatTo, $lte: dateISOFormatTo } },
            { date_to: { $gte: dateISOFormatTo, $lte: dateISOFormatTo } },
          ],
        })
          .populate({
            path: "members",
          })
          .exec(async (err, appointment) => {
            if (err) successWithSomeError = true
            if (appointment) {
              let isMemberExist = false;
              let memberName = "";


              for (i = 0; i < appointment.members.length; i++) {
                for (j = 0; j < req.body.members.length; j++) {
                  if (appointment.members[i]._id.toString() === req.body.members[j]) {
                    memberName = `${appointment.members[i].first_name} ${appointment.members[i].last_name}`;
                    isMemberExist = true;
                  }
                }
              }
              if (isMemberExist) {
                return successWithSomeError = true
              }
              if (appointment.driver_id.toString() === req.body.driver_id) {
                return successWithSomeError = true
              }
              if (appointment.vehicle_id.toString() === req.body.vehicle_id) {
                return successWithSomeError = true
              }
            }
            // console.log("index2", index);
            // console.log("dates.length", dates.length);

            // if (index + 1 !== dates.length && successWithSomeError === true) {
            //   return
            // } else if (index + 1 === dates.length && successWithSomeError === true) {
            //   return res.send({
            //     success: true,
            //     successWithSomeError
            //   })
            // }

            // console.log("dateISOFormatTo", dateISOFormatTo);
            const tempAptToIns = { ...req.body, date_from: dateISOFormatTo, date_to: dateISOFormatToTo }
            // console.log("tempAptToIns", tempAptToIns);
            const tempApt = await new Appointments(tempAptToIns);

            await tempApt.save((err, dateToInsert) => {
              if (err) successWithSomeError = true

            });


          });

      }))

      setTimeout(() => {
        res.send({
          success: true,
          successWithSomeError
        })
      }, 1500)

    }


  });





};

// Function to get one appointment data by ID
const getById = (req, res) => {
  Appointments.findOne({ _id: req.params.id })
    .populate({
      path: "vehicle_id",
      populate: {
        path: "category_color_id",
      },
    })
    .populate({
      path: "driver_id",
    })
    .populate({
      path: "members",
    })
    .populate({
      path: "category_color_id",
    })
    .exec((err, appointment) => {
      if (err)
        return res.send({
          success: false,
          appointment: appointment,
        });
      res.send({
        success: true,
        appointment: appointment,
      });
    });
};

// Function to delete all member data
const deleteAll = (req, res) => {
  Appointments.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All appointments have been removed." });
  });
};

// Function to delete one appointment data by ID
const deleteById = (req, res) => {
  // Remove one resource by id
  Appointments.remove({ _id: req.params.id }, (error, appointment) => {
    res.send({
      success: true,
      message: `Member with id: ${req.params.id} has been deleted`,
      data: appointment,
    });
  });
};

// Function to edit appointment data by ID
const editById = (req, res) => {
  // Create new resource object data
  const newAppointment = req.body;
  const startDate = new Date(req.body.date_from);
  const endDate = new Date(req.body.date_to);
  Appointments.findOne({
    $or: [
      { date_from: { $gte: startDate, $lte: endDate } },
      { date_to: { $gte: startDate, $lte: endDate } },
    ],
    _id: { $ne: req.params.id },
  })
    .populate({
      path: "members",
    })
    .exec((err, appointment) => {
      if (err)
        return res.send({
          success: false,
          message: err.message,
        });
      if (appointment) {
        let isMemberExist = false;
        let memberName = "";

        for (i = 0; i < appointment.members.length; i++) {
          for (j = 0; j < req.body.members.length; j++) {
            if (appointment.members[i]._id.toString() === req.body.members[j]) {
              memberName = `${appointment.members[i].first_name} ${appointment.members[i].last_name}`;
              isMemberExist = true;
            }
          }
        }
        if (isMemberExist) {
          return res.send({
            success: false,
            message: `Member already booked`,
          });
        }
        if (appointment.driver_id.toString() === req.body.driver_id) {
          return res.send({
            success: false,
            message: `Driver already booked`,
          });
        }
        if (appointment.vehicle_id.toString() === req.body.vehicle_id) {
          return res.send({
            success: false,
            message: `Vehicle already booked`,
          });
        }
      }
      Appointments.findByIdAndUpdate(
        { _id: req.params.id },
        newAppointment,
        { new: true },
        function (err, data) {
          if (err) {
            res.send({
              success: false,
              message: err.message,
            });
          } else {
            res.send({
              success: true,
              data: data,
            });
          }
        }
      );
    });
};

// Function to delete all member data
const getAppointmentsByMonth = (req, res) => {
  const month = parseInt(req.params.month)
  const year = parseInt(req.params.year)

  Appointments.aggregate([
    {
      $project: {
        name: 1,
        members: 1,
        vehicle_id: 1,
        driver_id: 1,
        date_from: 1,
        notes: 1,
        date_to: 1,
        month: { $month: "$date_from" },
        year: { $year: "$date_from" },
        dayOfWeek: { $dayOfWeek: "$date_from" },
        dayOfMonth: { $dayOfMonth: "$date_from" },
        dayOfMonth: { $dayOfMonth: "$date_from" },
      }
    },
    { $match: { month, year } },
    {
      $lookup: {
        from: 'members',
        localField: 'members',
        foreignField: '_id',
        as: 'members'
      }
    },
    {
      $lookup: {
        from: 'vehicles',
        localField: 'vehicle_id',
        foreignField: '_id',
        as: 'vehicle_id'
      }
    },
    {
      $lookup: {
        from: 'drivers',
        localField: 'driver_id',
        foreignField: '_id',
        as: 'driver_id'
      }
    },
    {
      $lookup: {
        from: 'category_colors',
        localField: 'vehicle_id.category_color_id',
        foreignField: '_id',
        as: 'color_detail'
      }
    }
  ])
    .exec((err, result) => {
      if (err) {
        res.send({
          success: false,
          message: err.message,
        });
      } else {
        res.send({
          success: true,
          data: result,
        });
      }
    });
};

// Export all functions
module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
  getAppointmentsByMonth,
};
