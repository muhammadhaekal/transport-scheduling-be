const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Set model name (table name / document name)
const modelName = "appointments";
// Set model structure (column list)
const schema = new Schema(
  {
    vehicle_id: {
      type: Schema.Types.ObjectId,
      ref: "vehicles",
    },
    driver_id: {
      type: Schema.Types.ObjectId,
      ref: "drivers",
    },
    category_color_id: {
      type: Schema.Types.ObjectId,
      ref: "category_colors",
    },
    name: {
      type: String,
      required: true,
    },
    notes: {
      type: String,
    },
    date_from: {
      type: Date,
      required: true,
    },
    date_to: {
      type: Date,
      required: true,
    },
    members: [
      {
        type: Schema.Types.ObjectId,
        ref: "members",
      },
    ],
  },
  { timestamps: true }
);

// Export the model
module.exports = mongoose.model(modelName, schema);
