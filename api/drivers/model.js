const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const modelName = "drivers";
const schema = new Schema(
  {
    first_name: {
      type: String,
    },
    last_name: {
      type: String,
    },
    birth_date: {
      type: Date,
    },
    phone_num: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model(modelName, schema);
