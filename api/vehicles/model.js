const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Set model name (table name / document name)
const modelName = "vehicles";
// Set model structure (column list)
const schema = new Schema(
  {
    category_color_id: {
      type: Schema.Types.ObjectId,
      ref: "category_colors",
    },
    license_plate_num: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

// Export the model
module.exports = mongoose.model(modelName, schema);
