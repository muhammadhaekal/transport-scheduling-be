// Import model from ./model.js
const Vehicles = require("./model");

// Function to get all vehicle
const getAll = (req, res) => {
  Vehicles.find({})
    .populate({
      path: "category_color_id",
    })
    .exec((err, vehicles) => {
      res.send({
        success: true,
        vehicles: vehicles,
      });
    });
};

// Function to insert one vehicle data
const createOne = (req, res) => {
  const newVehicle = new Vehicles(req.body);

  newVehicle.save((err, newData) => {
    if (err)
      return res.send({
        success: false,
        message: err.message,
      });

    res.send({
      new_vehicle: newData,
      success: true,
    });
  });
};

// Function to get one vehicle data by ID
const getById = (req, res) => {
  Vehicles.findOne({ _id: req.params.id }, (err, vehicle) => {
    res.send({
      success: true,
      vehicle: vehicle,
    });
  });
};

// Function to delete all member data
const deleteAll = (req, res) => {
  Vehicles.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All colors have been removed." });
  });
};

// Function to delete one member data by ID
const deleteById = (req, res) => {
  // Remove one resource by id
  Vehicles.remove({ _id: req.params.id }, (error, vehicle) => {
    res.send({
      success: true,
      message: `Member with id: ${req.params.id} has been deleted`,
      data: vehicle,
    });
  });
};

// Function to edit member data by ID
const editById = (req, res) => {
  // Create new resource object data
  const newVehicle = req.body;

  Vehicles.findByIdAndUpdate(
    { _id: req.params.id },
    newVehicle,
    { new: true },
    function (err, data) {
      if (err) {
        res.send({
          success: false,
          message: err.message,
        });
      } else {
        res.send({
          success: true,
          data: data,
        });
      }
    }
  );
};

// Export all functions
module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
};
