// Import model from ./model.js
const Members = require("./model");

// Function to get all members
const getAll = (req, res) => {
  Members.find({}).exec((err, members) => {
    res.send({
      success: true,
      members: members,
    });
  });
};

// Function to insert one member data
const createOne = (req, res) => {
  const newMember = new Members(req.body);

  newMember.save((err, newData) => {
    if (err)
      return res.send({
        success: false,
        message: err.message,
      });

    res.send({
      new_member: newData,
      success: true,
    });
  });
};

// Function to get one member data by ID
const getById = (req, res) => {
  Members.findOne({ _id: req.params.id }, (err, member) => {
    res.send({
      success: true,
      member: member,
    });
  });
};

// Function to delete all member data
const deleteAll = (req, res) => {
  CategoryColors.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All colors have been removed." });
  });
};

// Function to delete one member data by ID
const deleteById = (req, res) => {
  // Remove one resource by id
  Members.remove({ _id: req.params.id }, (error, member) => {
    res.send({
      success: true,
      message: `Member with id: ${req.params.id} has been deleted`,
      data: member,
    });
  });
};

// Function to edit member data by ID
const editById = (req, res) => {
  // Create new resource object data
  const newMember = req.body;

  Members.findByIdAndUpdate(
    { _id: req.params.id },
    newMember,
    { new: true },
    function (err, data) {
      if (err) {
        res.send({
          success: false,
          message: err.message,
        });
      } else {
        res.send({
          success: true,
          data: data,
        });
      }
    }
  );
};

// Export all functions
module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
};
