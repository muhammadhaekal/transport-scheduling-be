const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Set model name (table name / document name)
const modelName = "members";
// Set model structure (column list)
const schema = new Schema(
  {
    first_name: {
      type: String,
    },
    last_name: {
      type: String,
    },
    age: {
      type: Number,
    },
    birth_date: {
      type: Date,
    },
    phone_number: {
      type: String,
    },
    address: {
      type: String,
    },
  },
  { timestamps: true }
);

// Export the model
module.exports = mongoose.model(modelName, schema);
