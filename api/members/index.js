const express = require("express");
const router = express.Router();
// Import controller from ./controller.js file
const controller = require("./controller");

// handle request to GET localhost:3000/members (get all members)
router.get("/", controller.getAll);
// handle request to GET localhost:3000/members/:id (get member by id)
router.get("/:id", controller.getById);
// handle request to POST localhost:3000/members (insert one member data)
router.post("/", controller.createOne);
// handle request to PUT localhost:3000/members/:id (edit member data by id)
router.put("/:id", controller.editById);
// handle request to DELETE localhost:3000/members/:id (delete member data by id)
router.delete("/:id", controller.deleteById);
// handle request to DELETE localhost:3000/members (delete all member data)
router.delete("/", controller.deleteAll);

module.exports = router;
