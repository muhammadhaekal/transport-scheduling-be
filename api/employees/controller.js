const Employee = require("./model");
const nodemailer = require("nodemailer");

// Function to get all employee data
const getAll = (req, res) => {
  Employee.find({})
    .populate({
      path: "posts",
    })
    .exec((err, accounts) => {
      res.send({
        data: accounts,
      });
    });
};

// Function to insert one employee data
const createOne = (req, res) => {
  const newEmployee = new Employee(req.body);

  newEmployee.save((err) => {
    if (err) res.send("error");
    else {
      res.send({
        registered: newEmployee,
        success: true,
      });
    }
  });
};

// Function get one employee data by id
const getById = (req, res) => {
  Employee.findOne({ id: Number(req.params.id) }, (err, account) => {
    res.send({
      params: req.params,
      data: account,
    });
  });
};

// Function delete all employee data
const deleteAll = (req, res) => {
  Employee.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All accounts have been removed." });
  });
};

// Function delete employee data by id
const deleteById = (req, res) => {
  // Remove one resource by id
  Employee.findByIdAndRemove(req.params.id, (error, employees) => {
    if (employees !== null) {
      res.send({
        message: `Post with id: ${req.params.id} has been deleted`,
        data: employees,
      });
    } else {
      res.send({
        message: `Data not found`,
      });
    }
  });
};

// Function to edit employee data
const editById = (req, res) => {
  // Create new resource object data
  const newEmployee = req.body;

  Employee.findByIdAndUpdate({ _id: req.params.id }, newEmployee, function (
    err,
    result
  ) {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
};

// Function to handle login request
const login = (req, res) => {
  const body = req.body;
  Employee.findOne({ email: body.email, password: body.password }, function (
    err,
    employee
  ) {
    if (err !== null) {
      return res.send({
        success: false,
        message: "Error",
      });
    }

    if (employee == null) {
      return res.send({
        success: false,
        message: "Account not found or wrong password",
      });
    }

    res.send({
      success: true,
    });
  });
};

// Configure admil email
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "beyondblindnessadelaide@gmail.com",
    pass: "adelaide2020",
  },
});

// Function to handle request password
const resetPassword = (req, res) => {
  const body = req.body;

  Employee.findOne({ email: body.email }, (err, account) => {
    if (err || !account) {
      return res.send({
        success: false,
      });
    }

    const mailOptions = {
      from: "beyondblindnessadelaide@gmail.com", // sender address
      to: body.email, // list of receivers
      subject: "Reset Your Password", // Subject line
      html: `
      <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset Password</title>
</head>

<body>
    <div
        style="font-family: Arial, Helvetica, sans-serif;display: flex;justify-content: center;align-items: center;flex-direction: column;">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSySRvJKjWiEFbWPhiEJhLF1Gsu4peubVGsjpxpIQd78QzxhYb_FQ&s"
            alt="bb-img" style="width: 200px;height: auto;">
        <h2 style="color:rgb(29, 161, 242);">Click this link to reset your password</h2>
        <a style="text-decoration: none;" href="http://localhost:5500/change_password.html?email=${body.email}">
            <div
                style="background-color: #d4edda;color: #155721;padding: 5px 15px 5px 15px;border: none;border-radius: 50px;">
                Reset Password
            </div>
        </a>
    </div>

</body>

</html>
      `, // plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if (err) console.log(err);
      else console.log(info);
    });

    res.send({
      success: true,
      data: account,
    });
  });
};

// Function to handle change password
const changePassword = (req, res) => {
  const body = req.body;

  Employee.findOneAndUpdate(
    { email: body.email },
    { password: body.password },
    { new: true },
    (err, employee) => {
      if (err || !employee) {
        res.send({
          success: false,
        });
      }

      res.send({
        success: true,
        data: employee,
      });
    }
  );
};

// export all functions
module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
  login,
  resetPassword,
  changePassword,
};
