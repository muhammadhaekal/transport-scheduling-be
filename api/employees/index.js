const express = require("express");
const router = express.Router();
// import controller from ./controller.js
const controller = require("./controller");

// handle request to GET localhost:3000/employees (get all employees)
router.get("/", controller.getAll);
// handle request to GET localhost:3000/employees/:id (get one employee data by ID)
router.get("/:id", controller.getById);
// handle request to POST localhost:3000/employees/reset_password (handle reset password)
router.post("/reset_password", controller.resetPassword);
// handle request to POST localhost:3000/employees/login (handle login)
router.post("/login", controller.login);
// handle request to POST localhost:3000/employees (create employee data / sign up)
router.post("/", controller.createOne);
// handle request to PUT localhost:3000/employees (handle change password)
router.put("/change_password", controller.changePassword);
// handle request to PUT localhost:3000/change_password (update employee data by id)
router.put("/:id", controller.editById);
// handle request to DELETE localhost:3000/employees/:id (delete employee data by id)
router.delete("/:id", controller.deleteById);
// handle request to DELETE localhost:3000/employees (delete all employee data)
router.delete("/", controller.deleteAll);

module.exports = router;
