const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Set model name (table name / document name)
const modelName = "employee";
// Set model structure (column list)
const schema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
    },
    password: String,
    birth_date: Date,
    birth_place: String,
    job_title: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model(modelName, schema);
